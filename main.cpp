#include <windows.h>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <gdiplus.h>

#define BUTTON_ID 100

/* Define an index for the image handle */
#define IMAGE_HANDLE_INDEX GWLP_USERDATA

const char* greetings[] = {
	"愿你天天开心",
	"愿你梦想成真",
	"愿你生活幸福",
	"愿你事业有成",
	"愿你爱情甜蜜",
	"愿你健康长寿",
	"愿你财富滚滚",
	"愿你友情长存",
	"愿你家庭和睦",
	"愿你快乐常伴",
	"愿你平安喜乐",
	"愿你幸福美满",
	"愿你欢笑常在",
	"愿你好运连连",
	"愿你喜气洋洋",
	"愿你万事如意",
	"愿你心情舒畅",
	"愿你生活美满",
	"愿你快乐无忧",
	"愿你幸福安康",
	"愿你天天开心",
    "愿你梦想成真",
    "愿你生活幸福",
    "愿你事业有成",
    "愿你爱情甜蜜",
    "愿你健康长寿",
    "愿你财富滚滚",
    "愿你友情长存",
    "愿你家庭和睦",
    "愿你快乐常伴",
    "愿你平安喜乐",
    "愿你幸福美满",
    "愿你欢笑常在",
    "愿你好运连连",
    "愿你喜气洋洋",
    "愿你万事如意",
    "愿你心情舒畅",
    "愿你生活美满",
    "愿你快乐无忧",
    "愿你幸福安康",
    "笙磬同音，琴耽瑟好",
    "兰菊庭芳，天生一对",
    "和乐鱼水，鱼水相谐",
    "燕侣双俦，闺房和乐",
    "花开富贵，五世其昌",
    "琴瑟在御，琴瑟友之",
    "玉树琼枝，龙腾凤翔",
    "海燕双栖，乾坤定奏",
    "同德同心，诗咏关睢",
    "天地配合，成双成业",
    "夫妻偕老，同心和好",
    "鸡鸣戒旦，诗题红叶",
    "夫唱妇随，万年富贵",
    "有情成眷，诗咏好逑",
    "钟鼓乐之，鸳鸯比翼",
    "甜蜜佳缘，两情相悦",
    "凤翥龙翔，书称厘降",
    "乾坤和乐，燕尔新婚",
    "乐赋唱随，连理交枝",
    "于飞之乐，交颈鸳鸯",
    "中奖1元",
    "中奖5元",
    "中奖10元",
    "中奖100元"
};


POINT CenterWindow(HWND hwnd, int controlWidth, int controlHeight) {
	/* Get the size of the window's client area */
	RECT rect;
	GetClientRect(hwnd, &rect);
	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;

	/* Calculate the position of the control */
	POINT point;
	point.x = (width - controlWidth) / 2;
	point.y = (height - controlHeight) / 2;
	return point;
}

HBITMAP ResizeImage(HBITMAP hImage, int width, int height) {
	// 初始化GDI+
	Gdiplus::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	Gdiplus::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// 获取图片的宽度和高度
	BITMAP bm;
	GetObject(hImage, sizeof(bm), &bm);

	// 创建一个兼容的DC
	HDC hdc = GetDC(NULL);
	HDC hdcMem = CreateCompatibleDC(hdc);

	// 创建一个新的位图对象
	HBITMAP hNewImage = CreateCompatibleBitmap(hdc, width, height);

	// 选择位图对象到DC中
	HBITMAP hOldImage = (HBITMAP)SelectObject(hdcMem, hNewImage);

	// 使用GDI+来绘制缩放后的图片
	{
		Gdiplus::Graphics graphics(hdcMem);
		Gdiplus::Bitmap bitmap(hImage, NULL);
		graphics.SetInterpolationMode(Gdiplus::InterpolationModeHighQualityBicubic);
		graphics.DrawImage(&bitmap, 0, 0, width, height);
	}

	// 恢复DC的原始状态
	SelectObject(hdcMem, hOldImage);

	// 删除DC对象
	DeleteDC(hdcMem);
	ReleaseDC(NULL, hdc);

	// 关闭GDI+
	Gdiplus::GdiplusShutdown(gdiplusToken);

	return hNewImage;
}

/* This is where all the input to the window goes to */
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch(Message) {
		case WM_CREATE: {
			/* Initialize the random number generator */
			std::srand(std::time(NULL));

			/* Load the image */
			HANDLE hImage = LoadImage(NULL, "img/bg.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			if (hImage == NULL) {
				MessageBox(hwnd, "Failed to load image!", "Error", MB_ICONERROR);
				return -1;
			}

			/* Store the image handle in the window's extra data */
			SetWindowLongPtr(hwnd, IMAGE_HANDLE_INDEX, (LONG_PTR)hImage);

			/* Calculate the position of the button */
			POINT point = CenterWindow(hwnd, 100, 30);

			/* Create a button and add it to the window */
			CreateWindow("BUTTON", "Click me!", WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			             point.x, point.y, 100, 30, hwnd, (HMENU)BUTTON_ID, GetModuleHandle(NULL), NULL);
			break;
		}

		case WM_COMMAND: {
			/* Check if the button was clicked */
			if (LOWORD(wParam) == BUTTON_ID) {
				/* Choose a random greeting */

				/* Calculate the number of greetings */
				const int numGreetings = sizeof(greetings) / sizeof(greetings[0]);
				int index = std::rand() % numGreetings;
				const char* greeting = greetings[index];

				/* Show the greeting */
				MessageBox(hwnd, greeting, "Greeting", MB_ICONINFORMATION);
			}
			break;
		}
		case WM_SIZE: {
			// 获取窗口的新宽度和高度
			int width = LOWORD(lParam);
			int height = HIWORD(lParam);

			// 获取图片句柄
			HBITMAP hImage = (HBITMAP)GetWindowLongPtr(hwnd, IMAGE_HANDLE_INDEX);
			// 根据窗口的新大小调整图片的大小
			// 这里你可以使用自己的方法来调整图片的大小
			// 例如，你可以使用StretchBlt函数来缩放图片
			HBITMAP hNewImage = ResizeImage(hImage, width, height);
			// 将新的图片句柄存储到窗口的额外数据中
			SetWindowLongPtr(hwnd, IMAGE_HANDLE_INDEX, (LONG_PTR)hNewImage);
			// 删除旧的图片对象
			DeleteObject(hImage);
			// 重绘窗口
			InvalidateRect(hwnd, NULL, TRUE);
			break;
		}

		case WM_PAINT: {
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hwnd, &ps);

			/* Get the image handle from the window's extra data */
			HANDLE hImage = (HANDLE)GetWindowLongPtr(hwnd, IMAGE_HANDLE_INDEX);

			/* Draw the image */
			HDC hdcMem = CreateCompatibleDC(hdc);
			HGDIOBJ hOldBitmap = SelectObject(hdcMem, hImage);
			BITMAP bitmap;
			GetObject(hImage, sizeof(bitmap), &bitmap);
			BitBlt(hdc, 0, 0, bitmap.bmWidth, bitmap.bmHeight, hdcMem, 0, 0, SRCCOPY);
			SelectObject(hdcMem, hOldBitmap);
			DeleteDC(hdcMem);

			EndPaint(hwnd, &ps);
			break;
		}

		/* Upon destruction, tell the main thread to stop */
		case WM_DESTROY: {
			/* Get the image handle from the window's extra data */
			HANDLE hImage = (HANDLE)GetWindowLongPtr(hwnd, IMAGE_HANDLE_INDEX);

			/* Delete the image */
			DeleteObject(hImage);

			PostQuitMessage(0);
			break;
		}

		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}


/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	MSG msg; /* A temporary location for all messages */

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);

	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION); /* use the name "A" to use the project icon */

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	std::string title = "幸运抽奖程序";
	LPCSTR resultTitle = title.c_str();

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,"WindowClass",resultTitle,WS_VISIBLE | WS_OVERLAPPED | WS_SYSMENU ,
	                      CW_USEDEFAULT, /* x */
	                      CW_USEDEFAULT, /* y */
	                      640, /* width */
	                      480, /* height */
	                      NULL,NULL,hInstance,NULL);

	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	/*
		This is the heart of our program where all input is processed and
		sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
		this loop will not produce unreasonably high CPU usage
	*/
	while(GetMessage(&msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&msg); /* Translate key codes to chars if present */
		DispatchMessage(&msg); /* Send it to WndProc */
	}
	return msg.wParam;
}
